export interface ILabelType {
  id?: string;
  labelName?: string;
  labelDescription?: string;
  lock?: boolean;
  active?: boolean;
  taskDurationInMin?: number;
  comments?: string;
}

export const defaultValue: Readonly<ILabelType> = {
  lock: false,
  active: false,
};
