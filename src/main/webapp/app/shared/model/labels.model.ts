export interface ILabels {
  id?: string;
  label?: string;
  text?: string;
  comment?: string;
  source?: string;
  company?: string;
  lock?: boolean;
  active?: boolean;
  taskDurationInMin?: number;
}

export const defaultValue: Readonly<ILabels> = {
  lock: false,
  active: false,
};
