import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './labels.reducer';
import { ILabels } from 'app/shared/model/labels.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ILabelsUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const LabelsUpdate = (props: ILabelsUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { labelsEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/labels' + props.location.search);
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...labelsEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="taskCenterApp.labels.home.createOrEditLabel">Create or edit a Labels</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : labelsEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="labels-id">ID</Label>
                  <AvInput id="labels-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="labelLabel" for="labels-label">
                  Label
                </Label>
                <AvField id="labels-label" type="text" name="label" />
              </AvGroup>
              <AvGroup>
                <Label id="textLabel" for="labels-text">
                  Text
                </Label>
                <AvField id="labels-text" type="text" name="text" />
              </AvGroup>
              <AvGroup>
                <Label id="commentLabel" for="labels-comment">
                  Comment
                </Label>
                <AvField id="labels-comment" type="text" name="comment" />
              </AvGroup>
              <AvGroup>
                <Label id="sourceLabel" for="labels-source">
                  Source
                </Label>
                <AvField id="labels-source" type="text" name="source" />
              </AvGroup>
              <AvGroup>
                <Label id="companyLabel" for="labels-company">
                  Company
                </Label>
                <AvField id="labels-company" type="text" name="company" />
              </AvGroup>
              <AvGroup check>
                <Label id="lockLabel">
                  <AvInput id="labels-lock" type="checkbox" className="form-check-input" name="lock" />
                  Lock
                </Label>
              </AvGroup>
              <AvGroup check>
                <Label id="activeLabel">
                  <AvInput id="labels-active" type="checkbox" className="form-check-input" name="active" />
                  Active
                </Label>
              </AvGroup>
              <AvGroup>
                <Label id="taskDurationInMinLabel" for="labels-taskDurationInMin">
                  Task Duration In Min
                </Label>
                <AvField id="labels-taskDurationInMin" type="string" className="form-control" name="taskDurationInMin" />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/labels" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  labelsEntity: storeState.labels.entity,
  loading: storeState.labels.loading,
  updating: storeState.labels.updating,
  updateSuccess: storeState.labels.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LabelsUpdate);
