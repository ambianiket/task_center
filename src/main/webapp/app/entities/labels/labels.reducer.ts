import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ILabels, defaultValue } from 'app/shared/model/labels.model';

export const ACTION_TYPES = {
  FETCH_LABELS_LIST: 'labels/FETCH_LABELS_LIST',
  FETCH_LABELS: 'labels/FETCH_LABELS',
  CREATE_LABELS: 'labels/CREATE_LABELS',
  UPDATE_LABELS: 'labels/UPDATE_LABELS',
  DELETE_LABELS: 'labels/DELETE_LABELS',
  RESET: 'labels/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ILabels>,
  entity: defaultValue,
  updating: false,
  totalItems: 0,
  updateSuccess: false,
};

export type LabelsState = Readonly<typeof initialState>;

// Reducer

export default (state: LabelsState = initialState, action): LabelsState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_LABELS_LIST):
    case REQUEST(ACTION_TYPES.FETCH_LABELS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_LABELS):
    case REQUEST(ACTION_TYPES.UPDATE_LABELS):
    case REQUEST(ACTION_TYPES.DELETE_LABELS):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_LABELS_LIST):
    case FAILURE(ACTION_TYPES.FETCH_LABELS):
    case FAILURE(ACTION_TYPES.CREATE_LABELS):
    case FAILURE(ACTION_TYPES.UPDATE_LABELS):
    case FAILURE(ACTION_TYPES.DELETE_LABELS):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_LABELS_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
        totalItems: parseInt(action.payload.headers['x-total-count'], 10),
      };
    case SUCCESS(ACTION_TYPES.FETCH_LABELS):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_LABELS):
    case SUCCESS(ACTION_TYPES.UPDATE_LABELS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_LABELS):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/labels';

// Actions

export const getEntities: ICrudGetAllAction<ILabels> = (page, size, sort) => {
  const requestUrl = `${apiUrl}${sort ? `?page=${page}&size=${size}&sort=${sort}` : ''}`;
  return {
    type: ACTION_TYPES.FETCH_LABELS_LIST,
    payload: axios.get<ILabels>(`${requestUrl}${sort ? '&' : '?'}cacheBuster=${new Date().getTime()}`),
  };
};

export const getEntity: ICrudGetAction<ILabels> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_LABELS,
    payload: axios.get<ILabels>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<ILabels> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_LABELS,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ILabels> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_LABELS,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ILabels> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_LABELS,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
