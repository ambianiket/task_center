import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './labels.reducer';
import { ILabels } from 'app/shared/model/labels.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILabelsDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const LabelsDetail = (props: ILabelsDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { labelsEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          Labels [<b>{labelsEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="label">Label</span>
          </dt>
          <dd>{labelsEntity.label}</dd>
          <dt>
            <span id="text">Text</span>
          </dt>
          <dd>{labelsEntity.text}</dd>
          <dt>
            <span id="comment">Comment</span>
          </dt>
          <dd>{labelsEntity.comment}</dd>
          <dt>
            <span id="source">Source</span>
          </dt>
          <dd>{labelsEntity.source}</dd>
          <dt>
            <span id="company">Company</span>
          </dt>
          <dd>{labelsEntity.company}</dd>
          <dt>
            <span id="lock">Lock</span>
          </dt>
          <dd>{labelsEntity.lock ? 'true' : 'false'}</dd>
          <dt>
            <span id="active">Active</span>
          </dt>
          <dd>{labelsEntity.active ? 'true' : 'false'}</dd>
          <dt>
            <span id="taskDurationInMin">Task Duration In Min</span>
          </dt>
          <dd>{labelsEntity.taskDurationInMin}</dd>
        </dl>
        <Button tag={Link} to="/labels" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/labels/${labelsEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ labels }: IRootState) => ({
  labelsEntity: labels.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LabelsDetail);
