import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import Labels from './labels';
import LabelsDetail from './labels-detail';
import LabelsUpdate from './labels-update';
import LabelsDeleteDialog from './labels-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={LabelsUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={LabelsUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={LabelsDetail} />
      <ErrorBoundaryRoute path={match.url} component={Labels} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={LabelsDeleteDialog} />
  </>
);

export default Routes;
