import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import LabelType from './label-type';
import LabelTypeDetail from './label-type-detail';
import LabelTypeUpdate from './label-type-update';
import LabelTypeDeleteDialog from './label-type-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={LabelTypeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={LabelTypeUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={LabelTypeDetail} />
      <ErrorBoundaryRoute path={match.url} component={LabelType} />
    </Switch>
    <ErrorBoundaryRoute exact path={`${match.url}/:id/delete`} component={LabelTypeDeleteDialog} />
  </>
);

export default Routes;
