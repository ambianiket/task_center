import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
import { ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './label-type.reducer';
import { ILabelType } from 'app/shared/model/label-type.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILabelTypeProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export const LabelType = (props: ILabelTypeProps) => {
  useEffect(() => {
    props.getEntities();
  }, []);

  const { labelTypeList, match, loading } = props;
  return (
    <div>
      <h2 id="label-type-heading">
        Label Types
        <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
          <FontAwesomeIcon icon="plus" />
          &nbsp; Create new Label Type
        </Link>
      </h2>
      <div className="table-responsive">
        {labelTypeList && labelTypeList.length > 0 ? (
          <Table responsive>
            <thead>
              <tr>
                <th>ID</th>
                <th>Label Name</th>
                <th>Label Description</th>
                <th>Lock</th>
                <th>Active</th>
                <th>Task Duration In Min</th>
                <th>Comments</th>
                <th />
              </tr>
            </thead>
            <tbody>
              {labelTypeList.map((labelType, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${labelType.id}`} color="link" size="sm">
                      {labelType.id}
                    </Button>
                  </td>
                  <td>{labelType.labelName}</td>
                  <td>{labelType.labelDescription}</td>
                  <td>{labelType.lock ? 'true' : 'false'}</td>
                  <td>{labelType.active ? 'true' : 'false'}</td>
                  <td>{labelType.taskDurationInMin}</td>
                  <td>{labelType.comments}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${labelType.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" /> <span className="d-none d-md-inline">View</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${labelType.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${labelType.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" /> <span className="d-none d-md-inline">Delete</span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        ) : (
          !loading && <div className="alert alert-warning">No Label Types found</div>
        )}
      </div>
    </div>
  );
};

const mapStateToProps = ({ labelType }: IRootState) => ({
  labelTypeList: labelType.entities,
  loading: labelType.loading,
});

const mapDispatchToProps = {
  getEntities,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LabelType);
