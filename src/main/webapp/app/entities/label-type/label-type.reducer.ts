import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';

import { ILabelType, defaultValue } from 'app/shared/model/label-type.model';

export const ACTION_TYPES = {
  FETCH_LABELTYPE_LIST: 'labelType/FETCH_LABELTYPE_LIST',
  FETCH_LABELTYPE: 'labelType/FETCH_LABELTYPE',
  CREATE_LABELTYPE: 'labelType/CREATE_LABELTYPE',
  UPDATE_LABELTYPE: 'labelType/UPDATE_LABELTYPE',
  DELETE_LABELTYPE: 'labelType/DELETE_LABELTYPE',
  RESET: 'labelType/RESET',
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<ILabelType>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false,
};

export type LabelTypeState = Readonly<typeof initialState>;

// Reducer

export default (state: LabelTypeState = initialState, action): LabelTypeState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_LABELTYPE_LIST):
    case REQUEST(ACTION_TYPES.FETCH_LABELTYPE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true,
      };
    case REQUEST(ACTION_TYPES.CREATE_LABELTYPE):
    case REQUEST(ACTION_TYPES.UPDATE_LABELTYPE):
    case REQUEST(ACTION_TYPES.DELETE_LABELTYPE):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true,
      };
    case FAILURE(ACTION_TYPES.FETCH_LABELTYPE_LIST):
    case FAILURE(ACTION_TYPES.FETCH_LABELTYPE):
    case FAILURE(ACTION_TYPES.CREATE_LABELTYPE):
    case FAILURE(ACTION_TYPES.UPDATE_LABELTYPE):
    case FAILURE(ACTION_TYPES.DELETE_LABELTYPE):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload,
      };
    case SUCCESS(ACTION_TYPES.FETCH_LABELTYPE_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.FETCH_LABELTYPE):
      return {
        ...state,
        loading: false,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.CREATE_LABELTYPE):
    case SUCCESS(ACTION_TYPES.UPDATE_LABELTYPE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data,
      };
    case SUCCESS(ACTION_TYPES.DELETE_LABELTYPE):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {},
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};

const apiUrl = 'api/label-types';

// Actions

export const getEntities: ICrudGetAllAction<ILabelType> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_LABELTYPE_LIST,
  payload: axios.get<ILabelType>(`${apiUrl}?cacheBuster=${new Date().getTime()}`),
});

export const getEntity: ICrudGetAction<ILabelType> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_LABELTYPE,
    payload: axios.get<ILabelType>(requestUrl),
  };
};

export const createEntity: ICrudPutAction<ILabelType> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_LABELTYPE,
    payload: axios.post(apiUrl, cleanEntity(entity)),
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<ILabelType> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_LABELTYPE,
    payload: axios.put(apiUrl, cleanEntity(entity)),
  });
  return result;
};

export const deleteEntity: ICrudDeleteAction<ILabelType> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_LABELTYPE,
    payload: axios.delete(requestUrl),
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET,
});
