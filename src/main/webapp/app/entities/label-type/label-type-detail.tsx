import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
import { ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './label-type.reducer';
import { ILabelType } from 'app/shared/model/label-type.model';
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface ILabelTypeDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const LabelTypeDetail = (props: ILabelTypeDetailProps) => {
  useEffect(() => {
    props.getEntity(props.match.params.id);
  }, []);

  const { labelTypeEntity } = props;
  return (
    <Row>
      <Col md="8">
        <h2>
          LabelType [<b>{labelTypeEntity.id}</b>]
        </h2>
        <dl className="jh-entity-details">
          <dt>
            <span id="labelName">Label Name</span>
          </dt>
          <dd>{labelTypeEntity.labelName}</dd>
          <dt>
            <span id="labelDescription">Label Description</span>
          </dt>
          <dd>{labelTypeEntity.labelDescription}</dd>
          <dt>
            <span id="lock">Lock</span>
          </dt>
          <dd>{labelTypeEntity.lock ? 'true' : 'false'}</dd>
          <dt>
            <span id="active">Active</span>
          </dt>
          <dd>{labelTypeEntity.active ? 'true' : 'false'}</dd>
          <dt>
            <span id="taskDurationInMin">Task Duration In Min</span>
          </dt>
          <dd>{labelTypeEntity.taskDurationInMin}</dd>
          <dt>
            <span id="comments">Comments</span>
          </dt>
          <dd>{labelTypeEntity.comments}</dd>
        </dl>
        <Button tag={Link} to="/label-type" replace color="info">
          <FontAwesomeIcon icon="arrow-left" /> <span className="d-none d-md-inline">Back</span>
        </Button>
        &nbsp;
        <Button tag={Link} to={`/label-type/${labelTypeEntity.id}/edit`} replace color="primary">
          <FontAwesomeIcon icon="pencil-alt" /> <span className="d-none d-md-inline">Edit</span>
        </Button>
      </Col>
    </Row>
  );
};

const mapStateToProps = ({ labelType }: IRootState) => ({
  labelTypeEntity: labelType.entity,
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LabelTypeDetail);
