import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvFeedback, AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './label-type.reducer';
import { ILabelType } from 'app/shared/model/label-type.model';
import { convertDateTimeFromServer, convertDateTimeToServer, displayDefaultDateTime } from 'app/shared/util/date-utils';
import { mapIdList } from 'app/shared/util/entity-utils';

export interface ILabelTypeUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: string }> {}

export const LabelTypeUpdate = (props: ILabelTypeUpdateProps) => {
  const [isNew, setIsNew] = useState(!props.match.params || !props.match.params.id);

  const { labelTypeEntity, loading, updating } = props;

  const handleClose = () => {
    props.history.push('/label-type');
  };

  useEffect(() => {
    if (isNew) {
      props.reset();
    } else {
      props.getEntity(props.match.params.id);
    }
  }, []);

  useEffect(() => {
    if (props.updateSuccess) {
      handleClose();
    }
  }, [props.updateSuccess]);

  const saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const entity = {
        ...labelTypeEntity,
        ...values,
      };

      if (isNew) {
        props.createEntity(entity);
      } else {
        props.updateEntity(entity);
      }
    }
  };

  return (
    <div>
      <Row className="justify-content-center">
        <Col md="8">
          <h2 id="taskCenterApp.labelType.home.createOrEditLabel">Create or edit a LabelType</h2>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md="8">
          {loading ? (
            <p>Loading...</p>
          ) : (
            <AvForm model={isNew ? {} : labelTypeEntity} onSubmit={saveEntity}>
              {!isNew ? (
                <AvGroup>
                  <Label for="label-type-id">ID</Label>
                  <AvInput id="label-type-id" type="text" className="form-control" name="id" required readOnly />
                </AvGroup>
              ) : null}
              <AvGroup>
                <Label id="labelNameLabel" for="label-type-labelName">
                  Label Name
                </Label>
                <AvField id="label-type-labelName" type="text" name="labelName" />
              </AvGroup>
              <AvGroup>
                <Label id="labelDescriptionLabel" for="label-type-labelDescription">
                  Label Description
                </Label>
                <AvField id="label-type-labelDescription" type="text" name="labelDescription" />
              </AvGroup>
              <AvGroup check>
                <Label id="lockLabel">
                  <AvInput id="label-type-lock" type="checkbox" className="form-check-input" name="lock" />
                  Lock
                </Label>
              </AvGroup>
              <AvGroup check>
                <Label id="activeLabel">
                  <AvInput id="label-type-active" type="checkbox" className="form-check-input" name="active" />
                  Active
                </Label>
              </AvGroup>
              <AvGroup>
                <Label id="taskDurationInMinLabel" for="label-type-taskDurationInMin">
                  Task Duration In Min
                </Label>
                <AvField id="label-type-taskDurationInMin" type="string" className="form-control" name="taskDurationInMin" />
              </AvGroup>
              <AvGroup>
                <Label id="commentsLabel" for="label-type-comments">
                  Comments
                </Label>
                <AvField id="label-type-comments" type="text" name="comments" />
              </AvGroup>
              <Button tag={Link} id="cancel-save" to="/label-type" replace color="info">
                <FontAwesomeIcon icon="arrow-left" />
                &nbsp;
                <span className="d-none d-md-inline">Back</span>
              </Button>
              &nbsp;
              <Button color="primary" id="save-entity" type="submit" disabled={updating}>
                <FontAwesomeIcon icon="save" />
                &nbsp; Save
              </Button>
            </AvForm>
          )}
        </Col>
      </Row>
    </div>
  );
};

const mapStateToProps = (storeState: IRootState) => ({
  labelTypeEntity: storeState.labelType.entity,
  loading: storeState.labelType.loading,
  updating: storeState.labelType.updating,
  updateSuccess: storeState.labelType.updateSuccess,
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset,
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(LabelTypeUpdate);
