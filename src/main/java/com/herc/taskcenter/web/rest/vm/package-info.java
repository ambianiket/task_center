/**
 * View Models used by Spring MVC REST controllers.
 */
package com.herc.taskcenter.web.rest.vm;
