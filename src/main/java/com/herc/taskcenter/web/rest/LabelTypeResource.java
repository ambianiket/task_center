package com.herc.taskcenter.web.rest;

import com.herc.taskcenter.domain.LabelType;
import com.herc.taskcenter.repository.LabelTypeRepository;
import com.herc.taskcenter.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.herc.taskcenter.domain.LabelType}.
 */
@RestController
@RequestMapping("/api")
public class LabelTypeResource {

    private final Logger log = LoggerFactory.getLogger(LabelTypeResource.class);

    private static final String ENTITY_NAME = "labelType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LabelTypeRepository labelTypeRepository;

    public LabelTypeResource(LabelTypeRepository labelTypeRepository) {
        this.labelTypeRepository = labelTypeRepository;
    }

    /**
     * {@code POST  /label-types} : Create a new labelType.
     *
     * @param labelType the labelType to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new labelType, or with status {@code 400 (Bad Request)} if the labelType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/label-types")
    public ResponseEntity<LabelType> createLabelType(@RequestBody LabelType labelType) throws URISyntaxException {
        log.debug("REST request to save LabelType : {}", labelType);
        if (labelType.getId() != null) {
            throw new BadRequestAlertException("A new labelType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        LabelType result = labelTypeRepository.save(labelType);
        return ResponseEntity.created(new URI("/api/label-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /label-types} : Updates an existing labelType.
     *
     * @param labelType the labelType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated labelType,
     * or with status {@code 400 (Bad Request)} if the labelType is not valid,
     * or with status {@code 500 (Internal Server Error)} if the labelType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/label-types")
    public ResponseEntity<LabelType> updateLabelType(@RequestBody LabelType labelType) throws URISyntaxException {
        log.debug("REST request to update LabelType : {}", labelType);
        if (labelType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        LabelType result = labelTypeRepository.save(labelType);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, labelType.getId()))
            .body(result);
    }

    /**
     * {@code GET  /label-types} : get all the labelTypes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of labelTypes in body.
     */
    @GetMapping("/label-types")
    public List<LabelType> getAllLabelTypes() {
        log.debug("REST request to get all LabelTypes");
        return labelTypeRepository.findAll();
    }

    /**
     * {@code GET  /label-types/:id} : get the "id" labelType.
     *
     * @param id the id of the labelType to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the labelType, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/label-types/{id}")
    public ResponseEntity<LabelType> getLabelType(@PathVariable String id) {
        log.debug("REST request to get LabelType : {}", id);
        Optional<LabelType> labelType = labelTypeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(labelType);
    }

    /**
     * {@code DELETE  /label-types/:id} : delete the "id" labelType.
     *
     * @param id the id of the labelType to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/label-types/{id}")
    public ResponseEntity<Void> deleteLabelType(@PathVariable String id) {
        log.debug("REST request to delete LabelType : {}", id);
        labelTypeRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
