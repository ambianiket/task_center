package com.herc.taskcenter.web.rest;

import com.herc.taskcenter.domain.Labels;
import com.herc.taskcenter.repository.LabelsRepository;
import com.herc.taskcenter.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.herc.taskcenter.domain.Labels}.
 */
@RestController
@RequestMapping("/api")
public class LabelsResource {

    private final Logger log = LoggerFactory.getLogger(LabelsResource.class);

    private static final String ENTITY_NAME = "labels";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final LabelsRepository labelsRepository;

    public LabelsResource(LabelsRepository labelsRepository) {
        this.labelsRepository = labelsRepository;
    }

    /**
     * {@code POST  /labels} : Create a new labels.
     *
     * @param labels the labels to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new labels, or with status {@code 400 (Bad Request)} if the labels has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/labels")
    public ResponseEntity<Labels> createLabels(@RequestBody Labels labels) throws URISyntaxException {
        log.debug("REST request to save Labels : {}", labels);
        if (labels.getId() != null) {
            throw new BadRequestAlertException("A new labels cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Labels result = labelsRepository.save(labels);
        return ResponseEntity.created(new URI("/api/labels/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, false, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /labels} : Updates an existing labels.
     *
     * @param labels the labels to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated labels,
     * or with status {@code 400 (Bad Request)} if the labels is not valid,
     * or with status {@code 500 (Internal Server Error)} if the labels couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/labels")
    public ResponseEntity<Labels> updateLabels(@RequestBody Labels labels) throws URISyntaxException {
        log.debug("REST request to update Labels : {}", labels);
        if (labels.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Labels result = labelsRepository.save(labels);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, false, ENTITY_NAME, labels.getId()))
            .body(result);
    }

    /**
     * {@code GET  /labels} : get all the labels.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of labels in body.
     */
    @GetMapping("/labels")
    public ResponseEntity<List<Labels>> getAllLabels(Pageable pageable) {
        log.debug("REST request to get a page of Labels");
        Page<Labels> page = labelsRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /labels/:id} : get the "id" labels.
     *
     * @param id the id of the labels to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the labels, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/labels/{id}")
    public ResponseEntity<Labels> getLabels(@PathVariable String id) {
        log.debug("REST request to get Labels : {}", id);
        Optional<Labels> labels = labelsRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(labels);
    }

    /**
     * {@code DELETE  /labels/:id} : delete the "id" labels.
     *
     * @param id the id of the labels to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/labels/{id}")
    public ResponseEntity<Void> deleteLabels(@PathVariable String id) {
        log.debug("REST request to delete Labels : {}", id);
        labelsRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, false, ENTITY_NAME, id)).build();
    }
}
