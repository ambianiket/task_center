package com.herc.taskcenter.repository;

import com.herc.taskcenter.domain.LabelType;

import org.springframework.stereotype.Repository;

/**
 * Spring Data Couchbase repository for the LabelType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LabelTypeRepository extends N1qlCouchbaseRepository<LabelType, String> {
}
