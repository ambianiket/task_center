package com.herc.taskcenter.repository;

import com.herc.taskcenter.domain.Labels;

import org.springframework.stereotype.Repository;

/**
 * Spring Data Couchbase repository for the Labels entity.
 */
@SuppressWarnings("unused")
@Repository
public interface LabelsRepository extends N1qlCouchbaseRepository<Labels, String> {
}
