package com.herc.taskcenter.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.id.GeneratedValue;
import org.springframework.data.couchbase.core.mapping.id.IdPrefix;
import com.couchbase.client.java.repository.annotation.Field;

import java.io.Serializable;

import static com.herc.taskcenter.config.Constants.ID_DELIMITER;
import static org.springframework.data.couchbase.core.mapping.id.GenerationStrategy.UNIQUE;

/**
 * A LabelType.
 */
@Document
public class LabelType implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String PREFIX = "labeltype";

    @SuppressWarnings("unused")
    @IdPrefix
    private String prefix = PREFIX;

    @Id
    @GeneratedValue(strategy = UNIQUE, delimiter = ID_DELIMITER)
    private String id;

    @Field("label_name")
    private String labelName;

    @Field("label_description")
    private String labelDescription;

    @Field("lock")
    private Boolean lock;

    @Field("active")
    private Boolean active;

    @Field("task_duration_in_min")
    private Integer taskDurationInMin;

    @Field("comments")
    private String comments;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabelName() {
        return labelName;
    }

    public LabelType labelName(String labelName) {
        this.labelName = labelName;
        return this;
    }

    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    public String getLabelDescription() {
        return labelDescription;
    }

    public LabelType labelDescription(String labelDescription) {
        this.labelDescription = labelDescription;
        return this;
    }

    public void setLabelDescription(String labelDescription) {
        this.labelDescription = labelDescription;
    }

    public Boolean isLock() {
        return lock;
    }

    public LabelType lock(Boolean lock) {
        this.lock = lock;
        return this;
    }

    public void setLock(Boolean lock) {
        this.lock = lock;
    }

    public Boolean isActive() {
        return active;
    }

    public LabelType active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getTaskDurationInMin() {
        return taskDurationInMin;
    }

    public LabelType taskDurationInMin(Integer taskDurationInMin) {
        this.taskDurationInMin = taskDurationInMin;
        return this;
    }

    public void setTaskDurationInMin(Integer taskDurationInMin) {
        this.taskDurationInMin = taskDurationInMin;
    }

    public String getComments() {
        return comments;
    }

    public LabelType comments(String comments) {
        this.comments = comments;
        return this;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof LabelType)) {
            return false;
        }
        return id != null && id.equals(((LabelType) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "LabelType{" +
            "id=" + getId() +
            ", labelName='" + getLabelName() + "'" +
            ", labelDescription='" + getLabelDescription() + "'" +
            ", lock='" + isLock() + "'" +
            ", active='" + isActive() + "'" +
            ", taskDurationInMin=" + getTaskDurationInMin() +
            ", comments='" + getComments() + "'" +
            "}";
    }
}
