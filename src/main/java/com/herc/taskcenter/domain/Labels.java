package com.herc.taskcenter.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.couchbase.core.mapping.Document;
import org.springframework.data.couchbase.core.mapping.id.GeneratedValue;
import org.springframework.data.couchbase.core.mapping.id.IdPrefix;
import com.couchbase.client.java.repository.annotation.Field;

import java.io.Serializable;

import static com.herc.taskcenter.config.Constants.ID_DELIMITER;
import static org.springframework.data.couchbase.core.mapping.id.GenerationStrategy.UNIQUE;

/**
 * A Labels.
 */
@Document
public class Labels implements Serializable {

    private static final long serialVersionUID = 1L;
    public static final String PREFIX = "labels";

    @SuppressWarnings("unused")
    @IdPrefix
    private String prefix = PREFIX;

    @Id
    @GeneratedValue(strategy = UNIQUE, delimiter = ID_DELIMITER)
    private String id;

    @Field("label")
    private String label;

    @Field("text")
    private String text;

    @Field("comment")
    private String comment;

    @Field("source")
    private String source;

    @Field("company")
    private String company;

    @Field("lock")
    private Boolean lock;

    @Field("active")
    private Boolean active;

    @Field("task_duration_in_min")
    private Integer taskDurationInMin;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public Labels label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getText() {
        return text;
    }

    public Labels text(String text) {
        this.text = text;
        return this;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getComment() {
        return comment;
    }

    public Labels comment(String comment) {
        this.comment = comment;
        return this;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getSource() {
        return source;
    }

    public Labels source(String source) {
        this.source = source;
        return this;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getCompany() {
        return company;
    }

    public Labels company(String company) {
        this.company = company;
        return this;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Boolean isLock() {
        return lock;
    }

    public Labels lock(Boolean lock) {
        this.lock = lock;
        return this;
    }

    public void setLock(Boolean lock) {
        this.lock = lock;
    }

    public Boolean isActive() {
        return active;
    }

    public Labels active(Boolean active) {
        this.active = active;
        return this;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getTaskDurationInMin() {
        return taskDurationInMin;
    }

    public Labels taskDurationInMin(Integer taskDurationInMin) {
        this.taskDurationInMin = taskDurationInMin;
        return this;
    }

    public void setTaskDurationInMin(Integer taskDurationInMin) {
        this.taskDurationInMin = taskDurationInMin;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Labels)) {
            return false;
        }
        return id != null && id.equals(((Labels) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Labels{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", text='" + getText() + "'" +
            ", comment='" + getComment() + "'" +
            ", source='" + getSource() + "'" +
            ", company='" + getCompany() + "'" +
            ", lock='" + isLock() + "'" +
            ", active='" + isActive() + "'" +
            ", taskDurationInMin=" + getTaskDurationInMin() +
            "}";
    }
}
