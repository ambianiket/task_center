import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class LabelsUpdatePage {
  pageTitle: ElementFinder = element(by.id('taskCenterApp.labels.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  labelInput: ElementFinder = element(by.css('input#labels-label'));
  textInput: ElementFinder = element(by.css('input#labels-text'));
  commentInput: ElementFinder = element(by.css('input#labels-comment'));
  sourceInput: ElementFinder = element(by.css('input#labels-source'));
  companyInput: ElementFinder = element(by.css('input#labels-company'));
  lockInput: ElementFinder = element(by.css('input#labels-lock'));
  activeInput: ElementFinder = element(by.css('input#labels-active'));
  taskDurationInMinInput: ElementFinder = element(by.css('input#labels-taskDurationInMin'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setLabelInput(label) {
    await this.labelInput.sendKeys(label);
  }

  async getLabelInput() {
    return this.labelInput.getAttribute('value');
  }

  async setTextInput(text) {
    await this.textInput.sendKeys(text);
  }

  async getTextInput() {
    return this.textInput.getAttribute('value');
  }

  async setCommentInput(comment) {
    await this.commentInput.sendKeys(comment);
  }

  async getCommentInput() {
    return this.commentInput.getAttribute('value');
  }

  async setSourceInput(source) {
    await this.sourceInput.sendKeys(source);
  }

  async getSourceInput() {
    return this.sourceInput.getAttribute('value');
  }

  async setCompanyInput(company) {
    await this.companyInput.sendKeys(company);
  }

  async getCompanyInput() {
    return this.companyInput.getAttribute('value');
  }

  getLockInput() {
    return this.lockInput;
  }
  getActiveInput() {
    return this.activeInput;
  }
  async setTaskDurationInMinInput(taskDurationInMin) {
    await this.taskDurationInMinInput.sendKeys(taskDurationInMin);
  }

  async getTaskDurationInMinInput() {
    return this.taskDurationInMinInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setLabelInput('label');
    expect(await this.getLabelInput()).to.match(/label/);
    await waitUntilDisplayed(this.saveButton);
    await this.setTextInput('text');
    expect(await this.getTextInput()).to.match(/text/);
    await waitUntilDisplayed(this.saveButton);
    await this.setCommentInput('comment');
    expect(await this.getCommentInput()).to.match(/comment/);
    await waitUntilDisplayed(this.saveButton);
    await this.setSourceInput('source');
    expect(await this.getSourceInput()).to.match(/source/);
    await waitUntilDisplayed(this.saveButton);
    await this.setCompanyInput('company');
    expect(await this.getCompanyInput()).to.match(/company/);
    await waitUntilDisplayed(this.saveButton);
    const selectedLock = await this.getLockInput().isSelected();
    if (selectedLock) {
      await this.getLockInput().click();
      expect(await this.getLockInput().isSelected()).to.be.false;
    } else {
      await this.getLockInput().click();
      expect(await this.getLockInput().isSelected()).to.be.true;
    }
    await waitUntilDisplayed(this.saveButton);
    const selectedActive = await this.getActiveInput().isSelected();
    if (selectedActive) {
      await this.getActiveInput().click();
      expect(await this.getActiveInput().isSelected()).to.be.false;
    } else {
      await this.getActiveInput().click();
      expect(await this.getActiveInput().isSelected()).to.be.true;
    }
    await waitUntilDisplayed(this.saveButton);
    await this.setTaskDurationInMinInput('5');
    expect(await this.getTaskDurationInMinInput()).to.eq('5');
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}
