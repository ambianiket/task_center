import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import LabelsUpdatePage from './labels-update.page-object';

const expect = chai.expect;
export class LabelsDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('taskCenterApp.labels.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-labels'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class LabelsComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('labels-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('labels');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateLabels() {
    await this.createButton.click();
    return new LabelsUpdatePage();
  }

  async deleteLabels() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const labelsDeleteDialog = new LabelsDeleteDialog();
    await waitUntilDisplayed(labelsDeleteDialog.deleteModal);
    expect(await labelsDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/taskCenterApp.labels.delete.question/);
    await labelsDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(labelsDeleteDialog.deleteModal);

    expect(await isVisible(labelsDeleteDialog.deleteModal)).to.be.false;
  }
}
