import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import LabelsComponentsPage from './labels.page-object';
import LabelsUpdatePage from './labels-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('Labels e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let labelsComponentsPage: LabelsComponentsPage;
  let labelsUpdatePage: LabelsUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    labelsComponentsPage = new LabelsComponentsPage();
    labelsComponentsPage = await labelsComponentsPage.goToPage(navBarPage);
  });

  it('should load Labels', async () => {
    expect(await labelsComponentsPage.title.getText()).to.match(/Labels/);
    expect(await labelsComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete Labels', async () => {
    const beforeRecordsCount = (await isVisible(labelsComponentsPage.noRecords)) ? 0 : await getRecordsCount(labelsComponentsPage.table);
    labelsUpdatePage = await labelsComponentsPage.goToCreateLabels();
    await labelsUpdatePage.enterData();

    expect(await labelsComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(labelsComponentsPage.table);
    await waitUntilCount(labelsComponentsPage.records, beforeRecordsCount + 1);
    expect(await labelsComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await labelsComponentsPage.deleteLabels();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(labelsComponentsPage.records, beforeRecordsCount);
      expect(await labelsComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(labelsComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
