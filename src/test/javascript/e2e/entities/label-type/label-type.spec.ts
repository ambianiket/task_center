import { browser, element, by } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import SignInPage from './../../page-objects/signin-page';
import LabelTypeComponentsPage from './label-type.page-object';
import LabelTypeUpdatePage from './label-type-update.page-object';
import {
  waitUntilDisplayed,
  waitUntilAnyDisplayed,
  click,
  getRecordsCount,
  waitUntilHidden,
  waitUntilCount,
  isVisible,
} from '../../util/utils';

const expect = chai.expect;

describe('LabelType e2e test', () => {
  let navBarPage: NavBarPage;
  let signInPage: SignInPage;
  let labelTypeComponentsPage: LabelTypeComponentsPage;
  let labelTypeUpdatePage: LabelTypeUpdatePage;

  before(async () => {
    await browser.get('/');
    navBarPage = new NavBarPage();
    signInPage = await navBarPage.getSignInPage();
    await signInPage.waitUntilDisplayed();

    await signInPage.username.sendKeys('admin');
    await signInPage.password.sendKeys('admin');
    await signInPage.loginButton.click();
    await signInPage.waitUntilHidden();
    await waitUntilDisplayed(navBarPage.entityMenu);
    await waitUntilDisplayed(navBarPage.adminMenu);
    await waitUntilDisplayed(navBarPage.accountMenu);
  });

  beforeEach(async () => {
    await browser.get('/');
    await waitUntilDisplayed(navBarPage.entityMenu);
    labelTypeComponentsPage = new LabelTypeComponentsPage();
    labelTypeComponentsPage = await labelTypeComponentsPage.goToPage(navBarPage);
  });

  it('should load LabelTypes', async () => {
    expect(await labelTypeComponentsPage.title.getText()).to.match(/Label Types/);
    expect(await labelTypeComponentsPage.createButton.isEnabled()).to.be.true;
  });

  it('should create and delete LabelTypes', async () => {
    const beforeRecordsCount = (await isVisible(labelTypeComponentsPage.noRecords))
      ? 0
      : await getRecordsCount(labelTypeComponentsPage.table);
    labelTypeUpdatePage = await labelTypeComponentsPage.goToCreateLabelType();
    await labelTypeUpdatePage.enterData();

    expect(await labelTypeComponentsPage.createButton.isEnabled()).to.be.true;
    await waitUntilDisplayed(labelTypeComponentsPage.table);
    await waitUntilCount(labelTypeComponentsPage.records, beforeRecordsCount + 1);
    expect(await labelTypeComponentsPage.records.count()).to.eq(beforeRecordsCount + 1);

    await labelTypeComponentsPage.deleteLabelType();
    if (beforeRecordsCount !== 0) {
      await waitUntilCount(labelTypeComponentsPage.records, beforeRecordsCount);
      expect(await labelTypeComponentsPage.records.count()).to.eq(beforeRecordsCount);
    } else {
      await waitUntilDisplayed(labelTypeComponentsPage.noRecords);
    }
  });

  after(async () => {
    await navBarPage.autoSignOut();
  });
});
