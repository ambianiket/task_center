import { element, by, ElementFinder } from 'protractor';
import { waitUntilDisplayed, waitUntilHidden, isVisible } from '../../util/utils';

const expect = chai.expect;

export default class LabelTypeUpdatePage {
  pageTitle: ElementFinder = element(by.id('taskCenterApp.labelType.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  labelNameInput: ElementFinder = element(by.css('input#label-type-labelName'));
  labelDescriptionInput: ElementFinder = element(by.css('input#label-type-labelDescription'));
  lockInput: ElementFinder = element(by.css('input#label-type-lock'));
  activeInput: ElementFinder = element(by.css('input#label-type-active'));
  taskDurationInMinInput: ElementFinder = element(by.css('input#label-type-taskDurationInMin'));
  commentsInput: ElementFinder = element(by.css('input#label-type-comments'));

  getPageTitle() {
    return this.pageTitle;
  }

  async setLabelNameInput(labelName) {
    await this.labelNameInput.sendKeys(labelName);
  }

  async getLabelNameInput() {
    return this.labelNameInput.getAttribute('value');
  }

  async setLabelDescriptionInput(labelDescription) {
    await this.labelDescriptionInput.sendKeys(labelDescription);
  }

  async getLabelDescriptionInput() {
    return this.labelDescriptionInput.getAttribute('value');
  }

  getLockInput() {
    return this.lockInput;
  }
  getActiveInput() {
    return this.activeInput;
  }
  async setTaskDurationInMinInput(taskDurationInMin) {
    await this.taskDurationInMinInput.sendKeys(taskDurationInMin);
  }

  async getTaskDurationInMinInput() {
    return this.taskDurationInMinInput.getAttribute('value');
  }

  async setCommentsInput(comments) {
    await this.commentsInput.sendKeys(comments);
  }

  async getCommentsInput() {
    return this.commentsInput.getAttribute('value');
  }

  async save() {
    await this.saveButton.click();
  }

  async cancel() {
    await this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }

  async enterData() {
    await waitUntilDisplayed(this.saveButton);
    await this.setLabelNameInput('labelName');
    expect(await this.getLabelNameInput()).to.match(/labelName/);
    await waitUntilDisplayed(this.saveButton);
    await this.setLabelDescriptionInput('labelDescription');
    expect(await this.getLabelDescriptionInput()).to.match(/labelDescription/);
    await waitUntilDisplayed(this.saveButton);
    const selectedLock = await this.getLockInput().isSelected();
    if (selectedLock) {
      await this.getLockInput().click();
      expect(await this.getLockInput().isSelected()).to.be.false;
    } else {
      await this.getLockInput().click();
      expect(await this.getLockInput().isSelected()).to.be.true;
    }
    await waitUntilDisplayed(this.saveButton);
    const selectedActive = await this.getActiveInput().isSelected();
    if (selectedActive) {
      await this.getActiveInput().click();
      expect(await this.getActiveInput().isSelected()).to.be.false;
    } else {
      await this.getActiveInput().click();
      expect(await this.getActiveInput().isSelected()).to.be.true;
    }
    await waitUntilDisplayed(this.saveButton);
    await this.setTaskDurationInMinInput('5');
    expect(await this.getTaskDurationInMinInput()).to.eq('5');
    await waitUntilDisplayed(this.saveButton);
    await this.setCommentsInput('comments');
    expect(await this.getCommentsInput()).to.match(/comments/);
    await this.save();
    await waitUntilHidden(this.saveButton);
    expect(await isVisible(this.saveButton)).to.be.false;
  }
}
