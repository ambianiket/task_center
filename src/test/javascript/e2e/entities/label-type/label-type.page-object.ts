import { element, by, ElementFinder, ElementArrayFinder } from 'protractor';

import { waitUntilAnyDisplayed, waitUntilDisplayed, click, waitUntilHidden, isVisible } from '../../util/utils';

import NavBarPage from './../../page-objects/navbar-page';

import LabelTypeUpdatePage from './label-type-update.page-object';

const expect = chai.expect;
export class LabelTypeDeleteDialog {
  deleteModal = element(by.className('modal'));
  private dialogTitle: ElementFinder = element(by.id('taskCenterApp.labelType.delete.question'));
  private confirmButton = element(by.id('jhi-confirm-delete-labelType'));

  getDialogTitle() {
    return this.dialogTitle;
  }

  async clickOnConfirmButton() {
    await this.confirmButton.click();
  }
}

export default class LabelTypeComponentsPage {
  createButton: ElementFinder = element(by.id('jh-create-entity'));
  deleteButtons = element.all(by.css('div table .btn-danger'));
  title: ElementFinder = element(by.id('label-type-heading'));
  noRecords: ElementFinder = element(by.css('#app-view-container .table-responsive div.alert.alert-warning'));
  table: ElementFinder = element(by.css('#app-view-container div.table-responsive > table'));

  records: ElementArrayFinder = this.table.all(by.css('tbody tr'));

  getDetailsButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-info.btn-sm'));
  }

  getEditButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-primary.btn-sm'));
  }

  getDeleteButton(record: ElementFinder) {
    return record.element(by.css('a.btn.btn-danger.btn-sm'));
  }

  async goToPage(navBarPage: NavBarPage) {
    await navBarPage.getEntityPage('label-type');
    await waitUntilAnyDisplayed([this.noRecords, this.table]);
    return this;
  }

  async goToCreateLabelType() {
    await this.createButton.click();
    return new LabelTypeUpdatePage();
  }

  async deleteLabelType() {
    const deleteButton = this.getDeleteButton(this.records.last());
    await click(deleteButton);

    const labelTypeDeleteDialog = new LabelTypeDeleteDialog();
    await waitUntilDisplayed(labelTypeDeleteDialog.deleteModal);
    expect(await labelTypeDeleteDialog.getDialogTitle().getAttribute('id')).to.match(/taskCenterApp.labelType.delete.question/);
    await labelTypeDeleteDialog.clickOnConfirmButton();

    await waitUntilHidden(labelTypeDeleteDialog.deleteModal);

    expect(await isVisible(labelTypeDeleteDialog.deleteModal)).to.be.false;
  }
}
