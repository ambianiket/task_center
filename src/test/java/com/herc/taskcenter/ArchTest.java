package com.herc.taskcenter;

import com.tngtech.archunit.core.domain.JavaClasses;
import com.tngtech.archunit.core.importer.ClassFileImporter;
import com.tngtech.archunit.core.importer.ImportOption;
import org.junit.jupiter.api.Test;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

class ArchTest {

    @Test
    void servicesAndRepositoriesShouldNotDependOnWebLayer() {

        JavaClasses importedClasses = new ClassFileImporter()
            .withImportOption(ImportOption.Predefined.DO_NOT_INCLUDE_TESTS)
            .importPackages("com.herc.taskcenter");

        noClasses()
            .that()
                .resideInAnyPackage("com.herc.taskcenter.service..")
            .or()
                .resideInAnyPackage("com.herc.taskcenter.repository..")
            .should().dependOnClassesThat()
                .resideInAnyPackage("..com.herc.taskcenter.web..")
        .because("Services and repositories should not depend on web layer")
        .check(importedClasses);
    }
}
