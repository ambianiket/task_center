package com.herc.taskcenter.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.herc.taskcenter.web.rest.TestUtil;

public class LabelsTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Labels.class);
        Labels labels1 = new Labels();
        labels1.setId("id1");
        Labels labels2 = new Labels();
        labels2.setId(labels1.getId());
        assertThat(labels1).isEqualTo(labels2);
        labels2.setId("id2");
        assertThat(labels1).isNotEqualTo(labels2);
        labels1.setId(null);
        assertThat(labels1).isNotEqualTo(labels2);
    }
}
