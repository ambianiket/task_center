package com.herc.taskcenter.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.herc.taskcenter.web.rest.TestUtil;

public class LabelTypeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(LabelType.class);
        LabelType labelType1 = new LabelType();
        labelType1.setId("id1");
        LabelType labelType2 = new LabelType();
        labelType2.setId(labelType1.getId());
        assertThat(labelType1).isEqualTo(labelType2);
        labelType2.setId("id2");
        assertThat(labelType1).isNotEqualTo(labelType2);
        labelType1.setId(null);
        assertThat(labelType1).isNotEqualTo(labelType2);
    }
}
