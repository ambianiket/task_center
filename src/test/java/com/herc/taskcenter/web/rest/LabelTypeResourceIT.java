package com.herc.taskcenter.web.rest;

import com.herc.taskcenter.TaskCenterApp;
import com.herc.taskcenter.domain.LabelType;
import com.herc.taskcenter.repository.LabelTypeRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.TestSecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LabelTypeResource} REST controller.
 */
@SpringBootTest(classes = TaskCenterApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class LabelTypeResourceIT {

    private static final String DEFAULT_LABEL_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LABEL_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LABEL_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_LABEL_DESCRIPTION = "BBBBBBBBBB";

    private static final Boolean DEFAULT_LOCK = false;
    private static final Boolean UPDATED_LOCK = true;

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    private static final Integer DEFAULT_TASK_DURATION_IN_MIN = 1;
    private static final Integer UPDATED_TASK_DURATION_IN_MIN = 2;

    private static final String DEFAULT_COMMENTS = "AAAAAAAAAA";
    private static final String UPDATED_COMMENTS = "BBBBBBBBBB";

    @Autowired
    private LabelTypeRepository labelTypeRepository;

    @Autowired
    private MockMvc restLabelTypeMockMvc;

    private LabelType labelType;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LabelType createEntity() {
        LabelType labelType = new LabelType()
            .labelName(DEFAULT_LABEL_NAME)
            .labelDescription(DEFAULT_LABEL_DESCRIPTION)
            .lock(DEFAULT_LOCK)
            .active(DEFAULT_ACTIVE)
            .taskDurationInMin(DEFAULT_TASK_DURATION_IN_MIN)
            .comments(DEFAULT_COMMENTS);
        return labelType;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static LabelType createUpdatedEntity() {
        LabelType labelType = new LabelType()
            .labelName(UPDATED_LABEL_NAME)
            .labelDescription(UPDATED_LABEL_DESCRIPTION)
            .lock(UPDATED_LOCK)
            .active(UPDATED_ACTIVE)
            .taskDurationInMin(UPDATED_TASK_DURATION_IN_MIN)
            .comments(UPDATED_COMMENTS);
        return labelType;
    }

    @BeforeEach
    public void initTest() {
        labelTypeRepository.deleteAll();
        labelType = createEntity();
    }

    @Test
    public void createLabelType() throws Exception {
        int databaseSizeBeforeCreate = labelTypeRepository.findAll().size();
        // Create the LabelType
        restLabelTypeMockMvc.perform(post("/api/label-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(labelType)))
            .andExpect(status().isCreated());

        // Validate the LabelType in the database
        SecurityContextHolder.setContext(TestSecurityContextHolder.getContext());
        List<LabelType> labelTypeList = labelTypeRepository.findAll();
        assertThat(labelTypeList).hasSize(databaseSizeBeforeCreate + 1);
        LabelType testLabelType = labelTypeList.get(labelTypeList.size() - 1);
        assertThat(testLabelType.getLabelName()).isEqualTo(DEFAULT_LABEL_NAME);
        assertThat(testLabelType.getLabelDescription()).isEqualTo(DEFAULT_LABEL_DESCRIPTION);
        assertThat(testLabelType.isLock()).isEqualTo(DEFAULT_LOCK);
        assertThat(testLabelType.isActive()).isEqualTo(DEFAULT_ACTIVE);
        assertThat(testLabelType.getTaskDurationInMin()).isEqualTo(DEFAULT_TASK_DURATION_IN_MIN);
        assertThat(testLabelType.getComments()).isEqualTo(DEFAULT_COMMENTS);
    }

    @Test
    public void createLabelTypeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = labelTypeRepository.findAll().size();

        // Create the LabelType with an existing ID
        labelType.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restLabelTypeMockMvc.perform(post("/api/label-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(labelType)))
            .andExpect(status().isBadRequest());

        // Validate the LabelType in the database
        SecurityContextHolder.setContext(TestSecurityContextHolder.getContext());
        List<LabelType> labelTypeList = labelTypeRepository.findAll();
        assertThat(labelTypeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllLabelTypes() throws Exception {
        // Initialize the database
        labelTypeRepository.save(labelType);

        // Get all the labelTypeList
        restLabelTypeMockMvc.perform(get("/api/label-types?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(labelType.getId())))
            .andExpect(jsonPath("$.[*].labelName").value(hasItem(DEFAULT_LABEL_NAME)))
            .andExpect(jsonPath("$.[*].labelDescription").value(hasItem(DEFAULT_LABEL_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].lock").value(hasItem(DEFAULT_LOCK.booleanValue())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].taskDurationInMin").value(hasItem(DEFAULT_TASK_DURATION_IN_MIN)))
            .andExpect(jsonPath("$.[*].comments").value(hasItem(DEFAULT_COMMENTS)));
    }
    
    @Test
    public void getLabelType() throws Exception {
        // Initialize the database
        labelTypeRepository.save(labelType);

        // Get the labelType
        restLabelTypeMockMvc.perform(get("/api/label-types/{id}", labelType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(labelType.getId()))
            .andExpect(jsonPath("$.labelName").value(DEFAULT_LABEL_NAME))
            .andExpect(jsonPath("$.labelDescription").value(DEFAULT_LABEL_DESCRIPTION))
            .andExpect(jsonPath("$.lock").value(DEFAULT_LOCK.booleanValue()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.taskDurationInMin").value(DEFAULT_TASK_DURATION_IN_MIN))
            .andExpect(jsonPath("$.comments").value(DEFAULT_COMMENTS));
    }
    @Test
    public void getNonExistingLabelType() throws Exception {
        // Get the labelType
        restLabelTypeMockMvc.perform(get("/api/label-types/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateLabelType() throws Exception {
        // Initialize the database
        labelTypeRepository.save(labelType);

        int databaseSizeBeforeUpdate = labelTypeRepository.findAll().size();

        // Update the labelType
        LabelType updatedLabelType = labelTypeRepository.findById(labelType.getId()).get();
        updatedLabelType
            .labelName(UPDATED_LABEL_NAME)
            .labelDescription(UPDATED_LABEL_DESCRIPTION)
            .lock(UPDATED_LOCK)
            .active(UPDATED_ACTIVE)
            .taskDurationInMin(UPDATED_TASK_DURATION_IN_MIN)
            .comments(UPDATED_COMMENTS);

        restLabelTypeMockMvc.perform(put("/api/label-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedLabelType)))
            .andExpect(status().isOk());

        // Validate the LabelType in the database
        SecurityContextHolder.setContext(TestSecurityContextHolder.getContext());
        List<LabelType> labelTypeList = labelTypeRepository.findAll();
        assertThat(labelTypeList).hasSize(databaseSizeBeforeUpdate);
        LabelType testLabelType = labelTypeList.get(labelTypeList.size() - 1);
        assertThat(testLabelType.getLabelName()).isEqualTo(UPDATED_LABEL_NAME);
        assertThat(testLabelType.getLabelDescription()).isEqualTo(UPDATED_LABEL_DESCRIPTION);
        assertThat(testLabelType.isLock()).isEqualTo(UPDATED_LOCK);
        assertThat(testLabelType.isActive()).isEqualTo(UPDATED_ACTIVE);
        assertThat(testLabelType.getTaskDurationInMin()).isEqualTo(UPDATED_TASK_DURATION_IN_MIN);
        assertThat(testLabelType.getComments()).isEqualTo(UPDATED_COMMENTS);
    }

    @Test
    public void updateNonExistingLabelType() throws Exception {
        int databaseSizeBeforeUpdate = labelTypeRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLabelTypeMockMvc.perform(put("/api/label-types")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(labelType)))
            .andExpect(status().isBadRequest());

        // Validate the LabelType in the database
        SecurityContextHolder.setContext(TestSecurityContextHolder.getContext());
        List<LabelType> labelTypeList = labelTypeRepository.findAll();
        assertThat(labelTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteLabelType() throws Exception {
        // Initialize the database
        labelTypeRepository.save(labelType);

        int databaseSizeBeforeDelete = labelTypeRepository.findAll().size();

        // Delete the labelType
        restLabelTypeMockMvc.perform(delete("/api/label-types/{id}", labelType.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        SecurityContextHolder.setContext(TestSecurityContextHolder.getContext());
        List<LabelType> labelTypeList = labelTypeRepository.findAll();
        assertThat(labelTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
