package com.herc.taskcenter.web.rest;

import com.herc.taskcenter.TaskCenterApp;
import com.herc.taskcenter.domain.Labels;
import com.herc.taskcenter.repository.LabelsRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.TestSecurityContextHolder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link LabelsResource} REST controller.
 */
@SpringBootTest(classes = TaskCenterApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class LabelsResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_TEXT = "AAAAAAAAAA";
    private static final String UPDATED_TEXT = "BBBBBBBBBB";

    private static final String DEFAULT_COMMENT = "AAAAAAAAAA";
    private static final String UPDATED_COMMENT = "BBBBBBBBBB";

    private static final String DEFAULT_SOURCE = "AAAAAAAAAA";
    private static final String UPDATED_SOURCE = "BBBBBBBBBB";

    private static final String DEFAULT_COMPANY = "AAAAAAAAAA";
    private static final String UPDATED_COMPANY = "BBBBBBBBBB";

    private static final Boolean DEFAULT_LOCK = false;
    private static final Boolean UPDATED_LOCK = true;

    private static final Boolean DEFAULT_ACTIVE = false;
    private static final Boolean UPDATED_ACTIVE = true;

    private static final Integer DEFAULT_TASK_DURATION_IN_MIN = 1;
    private static final Integer UPDATED_TASK_DURATION_IN_MIN = 2;

    @Autowired
    private LabelsRepository labelsRepository;

    @Autowired
    private MockMvc restLabelsMockMvc;

    private Labels labels;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Labels createEntity() {
        Labels labels = new Labels()
            .label(DEFAULT_LABEL)
            .text(DEFAULT_TEXT)
            .comment(DEFAULT_COMMENT)
            .source(DEFAULT_SOURCE)
            .company(DEFAULT_COMPANY)
            .lock(DEFAULT_LOCK)
            .active(DEFAULT_ACTIVE)
            .taskDurationInMin(DEFAULT_TASK_DURATION_IN_MIN);
        return labels;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Labels createUpdatedEntity() {
        Labels labels = new Labels()
            .label(UPDATED_LABEL)
            .text(UPDATED_TEXT)
            .comment(UPDATED_COMMENT)
            .source(UPDATED_SOURCE)
            .company(UPDATED_COMPANY)
            .lock(UPDATED_LOCK)
            .active(UPDATED_ACTIVE)
            .taskDurationInMin(UPDATED_TASK_DURATION_IN_MIN);
        return labels;
    }

    @BeforeEach
    public void initTest() {
        labelsRepository.deleteAll();
        labels = createEntity();
    }

    @Test
    public void createLabels() throws Exception {
        int databaseSizeBeforeCreate = labelsRepository.findAll().size();
        // Create the Labels
        restLabelsMockMvc.perform(post("/api/labels")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(labels)))
            .andExpect(status().isCreated());

        // Validate the Labels in the database
        SecurityContextHolder.setContext(TestSecurityContextHolder.getContext());
        List<Labels> labelsList = labelsRepository.findAll();
        assertThat(labelsList).hasSize(databaseSizeBeforeCreate + 1);
        Labels testLabels = labelsList.get(labelsList.size() - 1);
        assertThat(testLabels.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testLabels.getText()).isEqualTo(DEFAULT_TEXT);
        assertThat(testLabels.getComment()).isEqualTo(DEFAULT_COMMENT);
        assertThat(testLabels.getSource()).isEqualTo(DEFAULT_SOURCE);
        assertThat(testLabels.getCompany()).isEqualTo(DEFAULT_COMPANY);
        assertThat(testLabels.isLock()).isEqualTo(DEFAULT_LOCK);
        assertThat(testLabels.isActive()).isEqualTo(DEFAULT_ACTIVE);
        assertThat(testLabels.getTaskDurationInMin()).isEqualTo(DEFAULT_TASK_DURATION_IN_MIN);
    }

    @Test
    public void createLabelsWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = labelsRepository.findAll().size();

        // Create the Labels with an existing ID
        labels.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restLabelsMockMvc.perform(post("/api/labels")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(labels)))
            .andExpect(status().isBadRequest());

        // Validate the Labels in the database
        SecurityContextHolder.setContext(TestSecurityContextHolder.getContext());
        List<Labels> labelsList = labelsRepository.findAll();
        assertThat(labelsList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void getAllLabels() throws Exception {
        // Initialize the database
        labelsRepository.save(labels);

        // Get all the labelsList
        restLabelsMockMvc.perform(get("/api/labels?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(labels.getId())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].text").value(hasItem(DEFAULT_TEXT)))
            .andExpect(jsonPath("$.[*].comment").value(hasItem(DEFAULT_COMMENT)))
            .andExpect(jsonPath("$.[*].source").value(hasItem(DEFAULT_SOURCE)))
            .andExpect(jsonPath("$.[*].company").value(hasItem(DEFAULT_COMPANY)))
            .andExpect(jsonPath("$.[*].lock").value(hasItem(DEFAULT_LOCK.booleanValue())))
            .andExpect(jsonPath("$.[*].active").value(hasItem(DEFAULT_ACTIVE.booleanValue())))
            .andExpect(jsonPath("$.[*].taskDurationInMin").value(hasItem(DEFAULT_TASK_DURATION_IN_MIN)));
    }
    
    @Test
    public void getLabels() throws Exception {
        // Initialize the database
        labelsRepository.save(labels);

        // Get the labels
        restLabelsMockMvc.perform(get("/api/labels/{id}", labels.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(labels.getId()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.text").value(DEFAULT_TEXT))
            .andExpect(jsonPath("$.comment").value(DEFAULT_COMMENT))
            .andExpect(jsonPath("$.source").value(DEFAULT_SOURCE))
            .andExpect(jsonPath("$.company").value(DEFAULT_COMPANY))
            .andExpect(jsonPath("$.lock").value(DEFAULT_LOCK.booleanValue()))
            .andExpect(jsonPath("$.active").value(DEFAULT_ACTIVE.booleanValue()))
            .andExpect(jsonPath("$.taskDurationInMin").value(DEFAULT_TASK_DURATION_IN_MIN));
    }
    @Test
    public void getNonExistingLabels() throws Exception {
        // Get the labels
        restLabelsMockMvc.perform(get("/api/labels/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateLabels() throws Exception {
        // Initialize the database
        labelsRepository.save(labels);

        int databaseSizeBeforeUpdate = labelsRepository.findAll().size();

        // Update the labels
        Labels updatedLabels = labelsRepository.findById(labels.getId()).get();
        updatedLabels
            .label(UPDATED_LABEL)
            .text(UPDATED_TEXT)
            .comment(UPDATED_COMMENT)
            .source(UPDATED_SOURCE)
            .company(UPDATED_COMPANY)
            .lock(UPDATED_LOCK)
            .active(UPDATED_ACTIVE)
            .taskDurationInMin(UPDATED_TASK_DURATION_IN_MIN);

        restLabelsMockMvc.perform(put("/api/labels")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedLabels)))
            .andExpect(status().isOk());

        // Validate the Labels in the database
        SecurityContextHolder.setContext(TestSecurityContextHolder.getContext());
        List<Labels> labelsList = labelsRepository.findAll();
        assertThat(labelsList).hasSize(databaseSizeBeforeUpdate);
        Labels testLabels = labelsList.get(labelsList.size() - 1);
        assertThat(testLabels.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testLabels.getText()).isEqualTo(UPDATED_TEXT);
        assertThat(testLabels.getComment()).isEqualTo(UPDATED_COMMENT);
        assertThat(testLabels.getSource()).isEqualTo(UPDATED_SOURCE);
        assertThat(testLabels.getCompany()).isEqualTo(UPDATED_COMPANY);
        assertThat(testLabels.isLock()).isEqualTo(UPDATED_LOCK);
        assertThat(testLabels.isActive()).isEqualTo(UPDATED_ACTIVE);
        assertThat(testLabels.getTaskDurationInMin()).isEqualTo(UPDATED_TASK_DURATION_IN_MIN);
    }

    @Test
    public void updateNonExistingLabels() throws Exception {
        int databaseSizeBeforeUpdate = labelsRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restLabelsMockMvc.perform(put("/api/labels")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(labels)))
            .andExpect(status().isBadRequest());

        // Validate the Labels in the database
        SecurityContextHolder.setContext(TestSecurityContextHolder.getContext());
        List<Labels> labelsList = labelsRepository.findAll();
        assertThat(labelsList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteLabels() throws Exception {
        // Initialize the database
        labelsRepository.save(labels);

        int databaseSizeBeforeDelete = labelsRepository.findAll().size();

        // Delete the labels
        restLabelsMockMvc.perform(delete("/api/labels/{id}", labels.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        SecurityContextHolder.setContext(TestSecurityContextHolder.getContext());
        List<Labels> labelsList = labelsRepository.findAll();
        assertThat(labelsList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
